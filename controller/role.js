const models = require('../models');

exports.create_a_role_get = async (req, res) => {
    try {
        const role = await models.Role.create({
            role_name: req.body.role_name
        })
        //success response
        res.status(200)
            .send({
                status: true,
                message: 'role created successfully',
                result: role
            })
    } catch (error) { 
        if(error) throw new Error;        
    }
    
}

exports.create_a_role_post = async (req, res) => {
    try {
        const role = await models.Role.create({
            role_name: req.body.role_name
        })
        //success response
        res.status(200)
            .send({
                status: true,
                message: 'role created successfully',
                result: role
            })
    } catch (error) { 
        if(error) throw new Error;        
    }
}

exports.get_a_role_get = async (req, res) => {
    try {
        const role = await models.Role.findById( 
            req.params.role_id,
        );
            if(!role) throw new Error();
         
         //success response
         res.status(200)
            .send({
             status: true,
             result: role
         })

    } catch (error) {
        if(error) {
            console.log(error);
            throw new Error;
        }
    }

}

exports.get_all_roles_get = async (req, res) => {
    const role = await models.Role.findAll();
    res.status(200)
        .send({
            status: true,
            result: role
        })
}

exports.update_a_role_get = async (req, res) =>{
    
}

exports.update_a_role_post = async (req, res) =>{
    try {
        const role = await models.Role.update({
            role_name: req.body.role_name
        },{
            where:{
                id: req.params.role_id
            }
        })
         //success response
         res.status(200)
         .send({
             status: true,
             message: 'role updated successfully',
             result: role
         })
    } catch (error) {
        if(error) throw new Error; 
        
    }
}

exports.delete_a_role_get = async (req, res) =>{
    await models.Role.destroy({
        where: {
            id: req.params.role_id
        }
    })
     //success response
     res.status(200)
     .send({
         status: true,
         message: 'role deleted successfully'
     })
}

exports.delete_a_role_post = (req, res) =>{

}