const models = require('../models');

exports.create_a_current_business_get = async (req, res) => {
    try {
        const current_business = await models.Current_business.create({
            name: req.body.name
        })
        //success response
        res.status(200)
            .send({
                status: true,
                message: 'current_business created successfully',
                result: current_business
            })
    } catch (error) { 
        if(error) throw new Error;        
    }
    
}

exports.create_a_current_business_post = async (req, res) =>{
    try {
        const current_business = await models.Current_business.create({
            name: req.body.name
        })
        //success response
        res.status(200)
            .send({
                status: true,
                message: 'current_business created successfully',
                result: current_business
            })
    } catch (error) { 
        if(error) throw new Error;        
    }
}

exports.get_a_current_business_get = async (req, res) => {
    try {
        const current_business = await models.current_business.findById( 
            req.params.current_business_id,
        );
            if(!current_business) throw new Error();
         
         //success response
         res.status(200)
            .send({
             status: true,
             result: current_business
         })

    } catch (error) {
        if(error) {
            console.log(error);
            throw new Error;
        }
    }

}

exports.get_all_current_businesss_get = async (req, res) => {
    const current_business = await models.Current_business.findAll();
    res.status(200)
    .send({
        status: true,
        message: 'current_business created successfully',
        result: current_business
    })
}

exports.update_a_current_business_get = async (req, res) =>{
    
}

exports.update_a_current_business_post = async (req, res) =>{
    try {
        const current_business = await models.Current_business.update({
            name: req.body.name
        },{
            where:{
                id: req.params.current_business_id
            }
        })
         //success response
         res.status(200)
         .send({
             status: true,
             message: 'current_business updated successfully',
             result: current_business
         })
    } catch (error) {
        if(error) throw new Error; 
        
    }
}

exports.delete_a_current_business_get = async (req, res) =>{
    await models.Current_business.destroy({
        where: {
            id: req.params.current_business_id
        }
    })
     //success response
     res.status(200)
     .send({
         status: true,
         message: 'current_business deleted successfully'
     })
}

exports.delete_a_current_business_post = (req, res) =>{

}