const models = require('../models');


exports.create_a_post_get = async (req, res) => {
    
    
}

exports.create_a_post_post = async (req, res) =>{
    switch (req.body.current_business_id) {
        case req.body.current_business_id:
            try {
                const post = await models.Post.create({
                    post_title: req.body.post_title,
                    post_body: req.body.post_body,
                    user_id: req.body.user_id
                })

                let current_business_list = req.body.current_business_id;
    
    // check the size of the current_business list
    console.log(current_business_list.length);
    
      if (current_business_list.length == 1) {
         // check if we have that current_business in our database
         const current_business = await models.Current_business.findById(req.body.current_business_id);
         if (!current_business) {
          return res.status(400);
         }
         //otherwise add new entry inside PostCurrent_business table
         await post.addCurrent_business(current_business);
    }
    // Ok now lets do for more than 1 current_business, the hard bit.
    // if more than one current_business has been selected
    else {
    // Loop through all the ids in req.body.categories i.e. the selected categories
    await req.body.current_business_id.forEach(async (id) => {
        // check if all current_business selected are in the database
        const current_business = await models.Current_business.findById(id);
        if (!current_business) {
          return res.status(400);
        }
        // add to PostCurrent_business after
        await post.addCurrent_business(current_business);
        });
    }
                //success response
                res.status(200)
                    .send({
                        status: true,
                        message: 'post created successfully',
                        result: post
                    })
            } catch (error) { 
                if(error) throw new Error;        
            }
            break;
    
        default:
            try {
                const post = await models.Post.create({
                    post_title: req.body.post_title,
                    post_body: req.body.post_body,
                    user_id: req.body.user_id
                })
                //success response
                res.status(200)
                    .send({
                        status: true,
                        message: 'post created successfully',
                        result: post
                    })
            } catch (error) { 
                if(error) throw new Error;        
            }
            break;
    }
    
}

exports.get_a_post_get = async (req, res) => {
    try {
        const post = await models.Post.findById( 
            req.params.post_id,
            {
                include: [{
                    model: models.User,
                    attributes: [ 'id', 'first_name', 'last_name', 'username', 'email' ]
                }]
            }
        );
            if(!post) throw new Error();
         
         //success response
         res.status(200)
            .send({
             status: true,
             message: 'Post, populated with the associated user details',
             result: post
         })

    } catch (error) {
        if(error) {
            console.log(error);
            throw new Error;
        }
    }
}

exports.get_a_post_get_by_a_user = async (req, res) => {
    try {
        const user = await models.User.findById( 
            req.params.user_id,
            {
                include: [{
                    model: models.Post,
                    attributes: [ 'id', 'post_title', 'post_body']
                }]
            }
        );
            if(!user) {
                res.status(400)
                    .send({
                        status: false,
                        error: 'There is no such user.'
                    })
            }
         
         //success response
         res.status(200)
            .send({
             status: true,
             message: user.username + ', populated with the associated post details',
             result: user.posts
         })

    } catch (error) {
        if(error) {
            console.log(error);
            throw new Error;
        }
    }
}

exports.get_a_post_user_by_department = async (req, res) => {
    try {
        const user = await models.User.findById( 
            req.params.user_id,
            {
                include: [{
                    model: models.Post,
                    attributes: [ 'id', 'post_title', 'post_body']
                }]
            }
        );
            if(!user || (user.department_id != req.params.department_id)) {
                res.status(400)
                    .send({
                        status: false,
                        error: 'There is no such user.'
                    })
            }
         
         //success response
         res.status(200)
            .send({
             status: true,
             message: user.username + ', populated with the associated post details',
             result: user.posts
         })

    } catch (error) {
        if(error) {
            console.log(error);
            throw new Error;
        }
    }
}

exports.get_a_post_user_by_department_by_profile = async (req, res) => {
    try {
        const user = await models.User.findById( 
            req.params.user_id,
            {
                include: [{
                    model: models.Post,
                    attributes: [ 'id', 'post_title', 'post_body']
                }]
            }
        );
            if(!user || ((user.department_id != req.params.department_id) && (user.profile_id != req.paarams.profile_id))) {
                res.status(400)
                    .send({
                        status: false,
                        error: 'There is no such user.'
                    })
            }
         
         //success response
         res.status(200)
            .send({
             status: true,
             message: user.username + ', populated with the associated post details',
             result: user.posts
         })

    } catch (error) {
        if(error) {
            console.log(error);
            throw new Error;
        }
    }
}

exports.get_a_post_user_by_current_business = async (req, res) => {
    try {
        const user = await models.User.findById( 
            req.params.user_id,
            {
                include: [{
                    model: models.Post,
                    attributes: [ 'id', 'post_title', 'post_body']
                }]
            }
        );
            if(!user || (user.current_business_id != req.params.current_business_id)) {
                res.status(400)
                    .send({
                        status: false,
                        error: 'There is no such user.'
                    })
            }
         
         //success response
         res.status(200)
            .send({
             status: true,
             message: user.username + ', populated with the associated post details',
             result: user.posts
         })

    } catch (error) {
        if(error) {
            console.log(error);
            throw new Error;
        }
    }
}

exports.get_all_posts_get = async (req, res) => {
    const post = await models.Post.findAll();
    res.status(200)
        .send({
            status: true,
            result: post
        })
}


exports.update_a_post_get = async (req, res) =>{

}

exports.update_a_post_post = async (req, res) =>{
    try {
        const post = await models.Post.update({
            post_title: req.body.post_title,
            post_body: req.body.post_body
        },{
            where:{
                id: req.params.post_id
            }
        })
         //success response
         res.status(200)
         .send({
             status: true,
             message: 'post updated successfully',
             result: post
         })
    } catch (error) {
        if(error) throw new Error; 
        
    }
}

exports.delete_a_post_get = async (req, res) =>{
    await models.Post.destroy({
        where: {
            id: req.params.post_id
        }
    })
     //success response
     res.status(200)
     .send({
         status: true,
         message: 'post deleted successfully'
     })
}

exports.delete_a_post_post = (req, res) =>{

}