const models = require('../models');

exports.create_a_module_get = async (req, res) => {
    try {
        const module = await models.Module.create({
            name: req.body.name
        })
        //success response
        res.status(200)
            .send({
                status: true,
                message: 'module created successfully',
                result: module
            })
    } catch (error) { 
        if(error) throw new Error;        
    }
    
}

exports.create_a_module_post = async (req, res) =>{
    try {
        const module = await models.Module.create({
            name: req.body.name
        })
        //success response
        res.status(200)
            .send({
                status: true,
                message: 'module created successfully',
                result: module
            })
    } catch (error) { 
        if(error) throw new Error;        
    }
}

exports.get_a_module_get = async (req, res) => {
    try {
        const module = await models.Module.findById( 
            req.params.module_id,
        );
            if(!module) throw new Error();
         
         //success response
         res.status(200)
            .send({
             status: true,
             result: module
         })

    } catch (error) {
        if(error) {
            console.log(error);
            throw new Error;
        }
    }

}

exports.get_all_modules_get = async (req, res) => {
    const module = await models.Module.findAll();
    res.status(200)
    .send({
        status: true,
        result: module
    })
}

exports.update_a_module_get = async (req, res) =>{
    
}

exports.update_a_module_post = async (req, res) =>{
    try {
        const module = await models.Module.update({
            name: req.body.name
        },{
            where:{
                id: req.params.module_id
            }
        })
         //success response
         res.status(200)
         .send({
             status: true,
             message: 'module updated successfully',
             result: module
         })
    } catch (error) {
        if(error) throw new Error; 
        
    }
}

exports.delete_a_module_get = async (req, res) =>{
    await models.Module.destroy({
        where: {
            id: req.params.module_id
        }
    })
     //success response
     res.status(200)
     .send({
         status: true,
         message: 'module deleted successfully'
     })
}

exports.delete_a_module_post = (req, res) =>{

}