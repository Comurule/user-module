const models = require('../models');

exports.create_a_profile_get = async (req, res) => {
    try {
        const profile = await models.Profile.create({
            profile_name: req.body.profile_name
        })
        //success response
        res.status(200)
            .send({
                status: true,
                message: 'profile created successfully',
                result: profile
            })
    } catch (error) { 
        if(error) throw new Error;        
    }
    
}

exports.create_a_profile_post = async (req, res) =>{
    try {
        
        const profile = await models.Profile.create({
            profile_name: req.body.profile_name
        })
        //success response
        res.status(200)
            .send({
                status: true,
                message: 'profile created successfully',
                result: profile
            })
    } catch (error) { 
        if(error) throw new Error;        
    }
}

exports.get_a_profile_get = async (req, res) => {
    try {
        const profile = await models.Profile.findById( 
            req.params.profile_id,
        );
            if(!profile) throw new Error();
         
         //success response
         res.status(200)
            .send({
             status: true,
             result: profile
         })

    } catch (error) {
        if(error) {
            console.log(error);
            throw new Error;
        }
    }

}

exports.get_all_profiles_get = async (req, res) => {
    const profile = await models.Profile.findAll();
    res.status(200)
    .send({
        status: true,
        result: profile
    })
}

exports.update_a_profile_get = async (req, res) =>{
    
}

exports.update_a_profile_post = async (req, res) =>{
    try {
        const profile = await models.Profile.update({
            profile_name: req.body.profile_name
        },{
            where:{
                id: req.params.profile_id
            }
        })
         //success response
         res.status(200)
         .send({
             status: true,
             message: 'profile updated successfully',
             result: profile
         })
    } catch (error) {
        if(error) throw new Error; 
        
    }
}

exports.delete_a_profile_get = async (req, res) =>{
    await models.Profile.destroy({
        where: {
            id: req.params.profile_id
        }
    })
     //success response
     res.status(200)
     .send({
         status: true,
         message: 'profile deleted successfully'
     })
}

exports.delete_a_profile_post = (req, res) =>{

}