const models = require('../models');

exports.create_a_department_get = async (req, res) => {
    try {
        const department = await models.Department.create({
            department_name: req.body.name
        })
        //success response
        res.status(200)
            .send({
                status: true,
                message: 'department created successfully',
                result: department
            })
    } catch (error) { 
        if(error) throw new Error;        
    }
    
}

exports.create_a_department_post = async (req, res) =>{
    try {
        const department = await models.Department.create({
            department_name: req.body.department_name
        })
        //success response
        res.status(200)
            .send({
                status: true,
                message: 'department created successfully',
                result: department
            })
    } catch (error) { 
        if(error) throw new Error;        
    }
}

exports.get_a_department_get = async (req, res) => {
    try {
        const department = await models.Department.findById( 
            req.params.department_id,
        );
            if(!department) throw new Error();
         
         //success response
         res.status(200)
         .send({
             status: true,
             result: department
         })

    } catch (error) {
        if(error) {
            console.log(error);
            throw new Error;
        }
    }

}

exports.get_all_departments_get = async (req, res) => {
    const department = await models.Department.findAll();
    res.status(200)
    .send({
        status: true,
        message: 'department created successfully',
        result: department
    })
}

exports.update_a_department_get = async (req, res) =>{
    
}

exports.update_a_department_post = async (req, res) =>{
    try {
        const department = await models.Department.update({
            name: req.body.name
        },{
            where:{
                id: req.params.department_id
            }
        })
         //success response
         res.status(200)
         .send({
             status: true,
             message: 'department updated successfully',
             result: department
         })
    } catch (error) {
        if(error) throw new Error; 
        
    }
}

exports.delete_a_department_get = async (req, res) =>{
    await models.Department.destroy({
        where: {
            id: req.params.department_id
        }
    })
     //success response
     res.status(200)
     .send({
         status: true,
         message: 'department deleted successfully'
     })
}

exports.delete_a_department_post = (req, res) =>{

}