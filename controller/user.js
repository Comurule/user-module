const models = require('../models');

exports.create_a_user_get = async (req, res) => {
    let { first_name, last_name, username, email, password, account_id, 
        current_business_id, role_id, department_id, profile_id, module_id } = req.body;

    try {
        const user = await models.User.create({
            first_name, last_name, username, email, password, account_id, 
            current_business_id, role_id, department_id, profile_id, module_id
        })
        //success response
        res.status(200)
            .send({
                status: true,
                message: 'User created successfully',
                result: user
            })
    } catch (error) { 
        if(error) throw new Error;        
    }
    
}

exports.create_a_user_post = async (req, res) =>{
    let { first_name, last_name, username, email, password, account_id, 
        current_business_id, role_id, department_id, profile_id, module_id } = req.body;
        console.log({first_name, last_name, username, email, password, account_id,});
    try 
    {
        const user = await models.User.create({
            first_name, last_name, username, email, password, account_id, 
            current_business_id, role_id, department_id, profile_id, module_id
        })
        //success response
        res.status(200)
            .send({
                status: true,
                message: 'User created successfully',
                result: user
            })
    } 
    catch (error) { 
        if(error) {
            console.log(error);
            throw new Error;
        }        
    }
}

exports.get_a_user_get = async (req, res) => {
    try {
        const user = await models.User.findById( 
            req.params.user_id,
            {
                include: [
                    {
                        model: models.Role,
                        attributes: ['id', 'role_name']
                    },{
                        model: models.Profile,
                        attributes: ['id', 'profile_name']
                    },{
                        model: models.Post,
                        attributes: ['id', 'post_title', 'post_body']
                    },{
                        model: models.Module,
                        attributes: ['id', 'name']
                    },{
                        model: models.Department,
                        attributes: ['id', 'department_name']
                    },{
                        model: models.Current_business,
                        attributes: ['id', 'name']
                    }
                ]
            }
        );
            if(!user) throw new Error();
         
         //success response
         res.status(200)
         .send({
             status: true,
             message: user.first_name + user.last_name + ', popuplated with all associations',
             result: user
         })

    } catch (error) {
        if(error) {
            console.log(error);
            throw new Error;
        }
    }

}

exports.get_all_users_get = async (req, res) => {
    try 
    {
        const user = await models.User.findAll()
        if(!user) throw new Error();
        //success response
        res.status(200)
            .send({
                status: true,
                result: user
            })
    } 
    catch (error) { 
        if(error) {
            console.log(error);
            throw new Error;
        }        
    }
}

exports.update_a_user_get = (req, res) =>{

}

exports.update_a_user_post = async (req, res) =>{
    let { first_name, last_name, username, email, password, account_id, 
        current_business_id, role_id, department_id, profile_id, module_id } = req.body;
        console.log({first_name, last_name, username, email, password, account_id,});
    try 
    {
        const user = await models.User.update({
            first_name, last_name, username, email, password, account_id, 
            current_business_id, role_id, department_id, profile_id, module_id
        },{
            where: {
                id: req.params.user_id
            }
        })
        if(!user) throw new Error();
        //success response
        res.status(200)
            .send({
                status: true,
                message: 'User created successfully',
                result: user
            })
    } 
    catch (error) { 
        if(error) {
            console.log(error);
            throw new Error;
        }        
    }
}

exports.delete_a_user_get = async (req, res) =>{
    await models.User.destroy({
        where: {
            id: req.params.user_id
        }
    })
     //success response
     res.status(200)
     .send({
         status: true,
         message: 'user deleted successfully'
     })
}

exports.delete_a_user_post = (req, res) =>{

}