const router = require('express').Router();
const {
    create_a_current_business_post, create_a_current_business_get,
    update_a_current_business_get, update_a_current_business_post,
    delete_a_current_business_get, delete_a_current_business_post,
    get_a_current_business_get, get_all_current_businesss_get
} = require('../controller/current_business');

router.get('/create', create_a_current_business_get);
router.post('/create', create_a_current_business_post);

router.get('/:current_business_id/update', update_a_current_business_get);
router.post('/:current_business_id/update', update_a_current_business_post);

router.get('/:current_business_id/delete', delete_a_current_business_get);
router.post('/:current_business_id/delete', delete_a_current_business_post);

router.get('/:current_business_id', get_a_current_business_get);

router.get('/', get_all_current_businesss_get);

module.exports = router;