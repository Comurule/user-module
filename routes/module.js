const router = require('express').Router();
const {
    create_a_module_post, create_a_module_get,
    update_a_module_get, update_a_module_post,
    delete_a_module_get, delete_a_module_post,
    get_a_module_get, get_all_modules_get
} = require('../controller/module');

router.get('/create', create_a_module_get);
router.post('/create', create_a_module_post);

router.get('/:module_id/update', update_a_module_get);
router.post('/:module_id/update', update_a_module_post);

router.get('/:module_id/delete', delete_a_module_get);
router.post('/:module_id/delete', delete_a_module_post);

router.get('/:module_id', get_a_module_get);

router.get('/', get_all_modules_get);

module.exports = router;