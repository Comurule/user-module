const router = require('express').Router();
const {
    create_a_post_post, create_a_post_get,
    update_a_post_get, update_a_post_post,
    delete_a_post_get, delete_a_post_post,
    get_a_post_get, get_all_posts_get
} = require('../controller/post');

router.get('/create', create_a_post_get);
router.post('/create', create_a_post_post);

router.get('/:post_id/update', update_a_post_get);
router.post('/:post_id/update', update_a_post_post);

router.get('/:post_id/delete', delete_a_post_get);
router.post('/:post_id/delete', delete_a_post_post);

router.get('/:post_id', get_a_post_get);

router.get('/', get_all_posts_get);

module.exports = router;