const router = require('express').Router();
const {
    create_a_user_post, create_a_user_get,
    update_a_user_get, update_a_user_post,
    delete_a_user_get, delete_a_user_post,
    get_a_user_get, get_all_users_get
} = require('../controller/user');

router.get('/create', create_a_user_get);
router.post('/create', create_a_user_post);

router.get('/:user_id/update', update_a_user_get);
router.post('/:user_id/update', update_a_user_post);

router.get('/:user_id/delete', delete_a_user_get);
router.post('/:user_id/delete', delete_a_user_post);

router.get('/:user_id', get_a_user_get);

router.get('/', get_all_users_get);

module.exports = router;