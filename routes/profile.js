const router = require('express').Router();
const {
    create_a_profile_post, create_a_profile_get,
    update_a_profile_get, update_a_profile_post,
    delete_a_profile_get, delete_a_profile_post,
    get_a_profile_get, get_all_profiles_get
} = require('../controller/profile');

router.get('/create', create_a_profile_get);
router.post('/create', create_a_profile_post);

router.get('/:profile_id/update', update_a_profile_get);
router.post('/:profile_id/update', update_a_profile_post);

router.get('/:profile_id/delete', delete_a_profile_get);
router.post('/:profile_id/delete', delete_a_profile_post);

router.get('/:profile_id', get_a_profile_get);

router.get('/', get_all_profiles_get);

module.exports = router;