const router = require('express').Router();
const {
    create_a_role_post, create_a_role_get,
    update_a_role_get, update_a_role_post,
    delete_a_role_get, delete_a_role_post,
    get_a_role_get, get_all_roles_get
} = require('../controller/role');

router.get('/create', create_a_role_get);
router.post('/create', create_a_role_post);

router.get('/:role_id/update', update_a_role_get);
router.post('/:role_id/update', update_a_role_post);

router.get('/:role_id/delete', delete_a_role_get);
router.post('/:role_id/delete', delete_a_role_post);

router.get('/:role_id', get_a_role_get);

router.get('/', get_all_roles_get);

module.exports = router;