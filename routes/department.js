const router = require('express').Router();
const {
    create_a_department_post, create_a_department_get,
    update_a_department_get, update_a_department_post,
    delete_a_department_get, delete_a_department_post,
    get_a_department_get, get_all_departments_get
} = require('../controller/department');

router.get('/create', create_a_department_get);
router.post('/create', create_a_department_post);

router.get('/:department_id/update', update_a_department_get);
router.post('/:department_id/update', update_a_department_post);

router.get('/:department_id/delete', delete_a_department_get);
router.post('/:department_id/delete', delete_a_department_post);

router.get('/:department_id', get_a_department_get);

router.get('/', get_all_departments_get);

module.exports = router;