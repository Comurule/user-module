const router = require('express').Router();

const user = require('./user');
const post = require('./post');
const role = require('./role');
const profile = require('./profile');
const module_router = require('./module');
const department = require('./department');
const current_business = require('./current_business');

const { 
    get_a_post_get_by_a_user, 
    get_a_post_user_by_department, 
    get_a_post_user_by_department_by_profile, 
    get_a_post_user_by_current_business 
} = require('../controller/post');

//special routes
router.get('/posts/:user_id', get_a_post_get_by_a_user)
router.get('/posts/:user_id/:department_id', get_a_post_user_by_department)
router.get('/posts/:user_id/:department_id/:profile_id', get_a_post_user_by_department_by_profile)
router.get('/posts/:user_id/:current_business_id', get_a_post_user_by_current_business)

//normal routes
router.use('/users', user);
router.use('/posts', post);
router.use('/roles', role);
router.use('/profiles', profile);
router.use('/modules', module_router);
router.use('/departments', department);
router.use('/current_business', current_business);

module.exports = router;