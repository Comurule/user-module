'use strict';
module.exports = (sequelize, DataTypes) => {
  const Module = sequelize.define('Module', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
        validate: {
          notEmpty: true,
          }
    }
  }, {
    underscored: true,
  });
  Module.associate = (models) =>{
    // associations can be defined here
    models.Module.hasMany(models.User);
  };

  return Module;
};