'use strict';
module.exports = (sequelize, DataTypes) => {
  const Department = sequelize.define('Department', {
    department_name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      }
    }
  }, {
    underscored: true,
  });
  Department.associate = function(models) {
    // associations can be defined here
    models.Department.hasMany(models.User);
    
    models.Department.belongsToMany(models.Role,{ 
      as: 'departments', 
      through: 'RoleDepartment',
      foreignKey: 'department_id'
    });
    
    models.Department.belongsToMany(models.Profile,{ 
      as: 'departments', 
      through: 'ProfileDepartment',
      foreignKey: 'department_id'
    });
  };
  return Department;
};