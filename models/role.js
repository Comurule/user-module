'use strict';
module.exports = (sequelize, Datatypes) => {
  const Role = sequelize.define('Role', {
    role_name: {
      type: Datatypes.STRING,
      allowNull: false,
       validate: {
          notEmpty: true,
      }
    }
  }, {
    underscored: true,
  });
  Role.associate = (models) => {
    //associations can be defined here
    models.Role.hasMany(models.User);
    
     models.Role.belongsToMany(models.Department,{ 
      as: 'roles', 
      through: 'RoleDepartment',
      foreignKey: 'role_id'
    });
    
  };
  return Role;
};