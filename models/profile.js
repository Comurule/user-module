'use strict';
module.exports = (sequelize, DataTypes) => {
  const Profile = sequelize.define('Profile', {
    profile_name: {
      type: DataTypes.STRING,
      allowNull: false,
        validate: {
            notEmpty: true,
        }
    }
  }, {
    underscored: true
  });
  Profile.associate = function(models) {
    // associations can be defined here
    models.Profile.hasMany(models.User);
  
    models.Profile.belongsToMany(models.Department,{ 
      as: 'profiles', 
      through: 'ProfileDepartment',
      foreignKey: 'profile_id'
    });
 }
  return Profile;
};