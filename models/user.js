'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    first_name: {
      type: DataTypes.STRING,
      allowNull: true,
            validate:{
                isAlpha: true,
            }
    },
    last_name: {
      type: DataTypes.STRING,
      allowNull: true,
            validate:{
                isAlpha: true,
            }
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false,
            validate: {
              len: [8, 50], // must be between 8 and 50.
              notEmpty: true,
            }
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
          notEmpty: true,
          isEmail: true,
          isLowercase: true,
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
        validate: {
          notEmpty: true,
          isAlphanumeric: true,
          len: [8,20], //password cant be less than 8 characters                    
        }
    },
    last_login: {
      type: DataTypes.DATE,
    },
    status: {
      type: DataTypes.ENUM('active', 'inactive'),
        defaultValue: 'active'
    },
    account_id: {
      type: DataTypes.STRING,
    },
    permission: {
      type: DataTypes.STRING,
      validate: {
        isArray: true,
      }
    },
    
  }, {
    underscored: true,
  });
  User.associate = function(models) {
    // associations can be defined here
    models.User.hasMany(models.Post);
    
    models.User.belongsTo(models.Department, {
      foreignKey: {
        allowNull: false,
      }
    });
    
    models.User.belongsTo(models.Profile, {
      foreignKey: {
        allowNull: false,
      }
    });
    
    models.User.belongsTo(models.Role, {
      foreignKey: {
        allowNull: false,
      }
    });

    models.User.belongsTo(models.Module, {
      foreignKey: {
        allowNull: false,
      }
    });

    models.User.belongsTo(models.Current_business, {
      foreignKey: {
        allowNull: false,
      }
    });
  };
  return User;
};