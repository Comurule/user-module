'use strict';
module.exports = (sequelize, DataTypes) => {
  const Current_business = sequelize.define('Current_business', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      }
    }
  }, {
    underscored: true,
  });
  Current_business.associate = (models) => {
    // associations can be defined here
    models.Current_business.hasMany(models.User);

    models.Current_business.belongsToMany(models.Post,{ 
      as: 'current_businesses', 
      through: 'PostCurrent_business',
      foreignKey: 'current_business_id'
    });
  };
  return Current_business;
};