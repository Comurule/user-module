'use strict';
module.exports = (sequelize, DataTypes) => {
  const Post = sequelize.define('Post', {
    post_title: {
      type: DataTypes.STRING,
      allowNull: false,
       validate: {
          notEmpty: true,
      }
    },
    post_body: {
      type: DataTypes.TEXT,
      allowNull: false,
       validate: {
          notEmpty: true,
      }
    }
  }, {
    underscored: true,
  });
  Post.associate = (models) => {
    // associations can be defined here
    models.Post.belongsTo(models.User, {
      onDelete: "CASCADE ",
      foreignKey: {
        allowNull: false,
      }
    });

    models.Post.belongsToMany(models.Current_business,{ 
      as: 'posts', 
      through: 'PostCurrent_business',
      foreignKey: 'post_id'
    });
  };
  return Post;
};